package com.alexey.test.model;

/**
 * Created by Alexey on 06.05.2016.
 */
public class VkGroup {

    private Integer id;
    private String name;
    private String screen_name;
    private Integer is_closed;
    private String type;
    private Integer is_admin;
    private Integer is_member;
    private String photo_50;
    private String photo_100;
    private String photo_200;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public Integer getIs_closed() {
        return is_closed;
    }

    public String getType() {
        return type;
    }

    public Integer getIs_admin() {
        return is_admin;
    }

    public Integer getIs_member() {
        return is_member;
    }

    public String getPhoto_50() {
        return photo_50;
    }

    public String getPhoto_100() {
        return photo_100;
    }

    public String getPhoto_200() {
        return photo_200;
    }
}
