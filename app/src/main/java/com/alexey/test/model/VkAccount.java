package com.alexey.test.model;

/**
 * Created by Alexey on 03.05.2016.
 */
public class VkAccount {

    private String accessToken;

    private String userId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
