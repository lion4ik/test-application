package com.alexey.test.model;

import java.util.List;

/**
 * Created by Alexey on 08.05.2016.
 */
public abstract class VkBaseEntityList<T> {

    private Integer count;

    private List<T> items;

    public List<T> getItems() {
        return items;
    }

    public int getCount() {
        return count;
    }
}
