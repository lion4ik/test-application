package com.alexey.test.model.market;

/**
 * Created by Alexey on 08.05.2016.
 */
public class VkGroupItem {

    private Integer id;
    private Integer owner_id;
    private String title;
    private String description;

    private Category category;
    private Price price;

    private Integer date;
    private String thumb_photo;
    private Integer availability;

    public String getTitle() {
        return title;
    }

    public String getPriceText() {
        return price.text;
    }

    public String getThumb() {
        return thumb_photo;
    }
}
