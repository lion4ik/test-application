package com.alexey.test.presenter;

import com.alexey.test.model.VkGroupItems;
import com.alexey.test.network.listener.ApiRequestListener;
import com.alexey.test.network.request.GroupItemsRequest;
import com.alexey.test.ui.view.GroupsItemActivityView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Alexey on 07.05.2016.
 */
public class GroupsItemsActivityPresenter extends MvpSpicedPresenter<GroupsItemActivityView> {

    public GroupsItemsActivityPresenter(SpiceManager spiceManager) {
        super(spiceManager);
    }

    public void executeGroupItemsRequest(Integer owner_id) {
        GroupItemsRequest groupItemsRequest = new GroupItemsRequest(owner_id);
        GroupItemsListener groupItemsListener = new GroupItemsListener();
        getSpiceManager().execute(groupItemsRequest, groupItemsListener);
    }

    private class GroupItemsListener extends ApiRequestListener<VkGroupItems> {

        @Override
        public void onDataReceived(VkGroupItems data) {
            getView().setGroupItems(data.getItems());
        }
    }
}
