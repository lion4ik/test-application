package com.alexey.test.presenter;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Alexey on 08.05.2016.
 */
public class MvpSpicedPresenter<V extends MvpView> extends MvpNullObjectBasePresenter<V> {
    private SpiceManager spiceManager;

    public MvpSpicedPresenter(SpiceManager spiceManager) {
        this.spiceManager = spiceManager;
    }

    public SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
