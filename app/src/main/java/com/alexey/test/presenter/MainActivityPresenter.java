package com.alexey.test.presenter;

import android.util.Log;

import com.alexey.test.model.VkGroupsList;
import com.alexey.test.network.listener.ApiRequestListener;
import com.alexey.test.network.request.GroupsSearchRequest;
import com.alexey.test.ui.view.MainActivityView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Alexey on 03.05.2016.
 */
public class MainActivityPresenter extends MvpSpicedPresenter<MainActivityView> {
    private static final String TAG = "MainActivityPresenter";
    private static final String QUERY = "Велосипеды";

    public MainActivityPresenter(SpiceManager spiceManager) {
        super(spiceManager);
    }

    public void executeGroupsSearchRequest() {
        GroupsSearchRequest groupsSearchRequest = new GroupsSearchRequest(QUERY);
        GroupsSearchListener groupsSearchListener = new GroupsSearchListener();
        getSpiceManager().execute(groupsSearchRequest, groupsSearchListener);
    }

    private class GroupsSearchListener extends ApiRequestListener<VkGroupsList> {

        @Override
        public void onDataReceived(VkGroupsList data) {
            Log.d(TAG, "onDataReceived");
            getView().setGroups(data.getItems());
        }

        @Override
        public void onError(String errorMessage) {

        }
    }

}
