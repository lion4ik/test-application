package com.alexey.test.presenter;

import com.alexey.test.App;
import com.alexey.test.manager.VkAccountManager;
import com.alexey.test.network.VkWebViewAuthClient;
import com.alexey.test.ui.view.LoginActivityView;
import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

/**
 * Created by Alexey on 03.05.2016.
 */
public class LoginActivityPresenter extends MvpNullObjectBasePresenter<LoginActivityView> {

    @Inject
    VkWebViewAuthClient vkWebViewAuthClient;

    @Inject
    VkAccountManager vkAccountManager;

    public LoginActivityPresenter() {
        App.getInstance().getAppComponent().inject(this);
        vkWebViewAuthClient.setAuthListener(new VkWebViewAuthClient.AuthListener() {
            @Override
            public void onStartLoading() {
                getView().showProgress();
            }

            @Override
            public void onFinishLoading() {
                getView().hideProgress();
            }

            @Override
            public void onAuthorized(String accessToken, String userId) {
                vkAccountManager.saveAccountData(accessToken, userId);
                getView().onAuthorized();
            }
        });
    }

    public void setOnAuthListener(VkWebViewAuthClient.AuthListener authListener) {
        vkWebViewAuthClient.setAuthListener(authListener);
    }

    public void authorizeVk() {
        if(vkAccountManager.hasSavedAccountData()){
            vkAccountManager.restoreAccountData();
            getView().onAuthorized();
        }else{
            getView().setWebViewClient(vkWebViewAuthClient);
            getView().loadVkAuthorizeUrl();
        }
    }
}
