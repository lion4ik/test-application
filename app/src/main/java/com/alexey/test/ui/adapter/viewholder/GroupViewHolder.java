package com.alexey.test.ui.adapter.viewholder;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexey.test.App;
import com.alexey.test.R;
import com.alexey.test.model.VkGroup;
import com.alexey.test.ui.adapter.GroupsRecyclerAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import butterknife.BindView;

/**
 * Created by Alexey on 06.05.2016.
 */
public class GroupViewHolder extends BaseItemViewHolder<VkGroup> implements View.OnClickListener {

    @BindView(R.id.ivAvatar)
    ImageView groupAvatar;

    @BindView(R.id.ivName)
    TextView groupName;

    private GroupsRecyclerAdapter.OnItemClickListener onItemClickListener;
    private VkGroup item;

    public GroupViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bindItem(VkGroup item) {
        this.item = item;
        itemView.setOnClickListener(this);
        groupName.setText(item.getName());
        Glide.with(App.getInstance()).load(item.getPhoto_100()).asBitmap().into(new BitmapImageViewTarget(groupAvatar) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(App.getInstance().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                groupAvatar.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(item, getAdapterPosition());
        }
    }

    public void bindItemWithClick(VkGroup item, GroupsRecyclerAdapter.OnItemClickListener itemClickListener) {
        bindItem(item);
        onItemClickListener = itemClickListener;
    }
}
