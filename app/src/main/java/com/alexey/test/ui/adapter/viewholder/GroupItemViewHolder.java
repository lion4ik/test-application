package com.alexey.test.ui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexey.test.App;
import com.alexey.test.R;
import com.alexey.test.model.market.VkGroupItem;
import com.bumptech.glide.Glide;

import butterknife.BindView;

/**
 * Created by Alexey on 08.05.2016.
 */
public class GroupItemViewHolder extends BaseItemViewHolder<VkGroupItem> {

    @BindView(R.id.ivGroupItemAvatar)
    ImageView ivGroupItemAvatar;

    @BindView(R.id.tvGroupItemName)
    TextView tvGroupItemName;

    @BindView(R.id.tvGroupItemPrice)
    TextView tvGroupItemPrice;

    public GroupItemViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bindItem(VkGroupItem item) {
        tvGroupItemName.setText(item.getTitle());
        tvGroupItemPrice.setText(item.getPriceText());
        Glide.with(App.getInstance()).load(item.getThumb()).asBitmap().into(ivGroupItemAvatar);
    }
}
