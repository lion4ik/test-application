package com.alexey.test.ui.view;

import com.alexey.test.model.VkGroup;
import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;

/**
 * Created by Alexey on 03.05.2016.
 */
public interface MainActivityView extends MvpView {

    void setGroups(List<VkGroup> groups);
}
