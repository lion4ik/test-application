package com.alexey.test.ui.activity;

import android.app.Activity;
import android.os.Bundle;

import com.alexey.test.network.NetworkService;
import com.octo.android.robospice.SpiceManager;

public class BaseMvpActivityHelper<T extends Activity> {

    protected final T activity;

    protected final SpiceManager spiceManager = new SpiceManager(NetworkService.class);

    public BaseMvpActivityHelper(T activity) {
        this.activity = activity;
    }

    public void onCreate(Bundle savedInstanceState) {
        spiceManager.start(activity);
    }

    public void onStart() {
        if (!spiceManager.isStarted()) {
            spiceManager.start(activity);
        }
    }

    public void onStop() {
        if (spiceManager.isStarted()) {
            spiceManager.shouldStop();
        }
    }

    public SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
