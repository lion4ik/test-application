package com.alexey.test.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alexey.test.R;
import com.alexey.test.model.VkGroup;
import com.alexey.test.ui.adapter.viewholder.GroupViewHolder;

/**
 * Created by Alexey on 06.05.2016.
 */
public class GroupsRecyclerAdapter extends FooterRecyclerAdapter<VkGroup> {

    private OnItemClickListener onItemClickListener;

    public GroupsRecyclerAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_ITEM) {
            ((GroupViewHolder) holder).bindItemWithClick(getItem(position), onItemClickListener);
        }
    }

    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new GroupViewHolder(inflater.inflate(R.layout.layout_group, parent, false));
    }

    public interface OnItemClickListener {
        void onItemClick(VkGroup group, int position);
    }
}
