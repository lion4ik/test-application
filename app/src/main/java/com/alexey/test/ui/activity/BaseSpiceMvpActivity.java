package com.alexey.test.ui.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;
import com.octo.android.robospice.SpiceManager;

public abstract class BaseSpiceMvpActivity <V extends MvpView, P extends MvpPresenter<V>> extends MvpActivity<V, P> {

    protected BaseMvpActivityHelper<? extends BaseSpiceMvpActivity> activityHelper;

    public BaseSpiceMvpActivity(){
        activityHelper = new BaseMvpActivityHelper<>(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        activityHelper.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        activityHelper.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        activityHelper.onStop();
    }

    @NonNull
    public SpiceManager getSpiceManager(){
        return activityHelper.getSpiceManager();
    }
}
