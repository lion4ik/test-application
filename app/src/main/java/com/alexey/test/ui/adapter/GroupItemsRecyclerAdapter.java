package com.alexey.test.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alexey.test.R;
import com.alexey.test.model.market.VkGroupItem;
import com.alexey.test.ui.adapter.viewholder.GroupItemViewHolder;

/**
 * Created by Alexey on 08.05.2016.
 */
public class GroupItemsRecyclerAdapter extends FooterRecyclerAdapter<VkGroupItem> {
    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new GroupItemViewHolder(inflater.inflate(R.layout.layout_group_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_ITEM) {
            ((GroupItemViewHolder) holder).bindItem(getItem(position));
        }
    }
}
