package com.alexey.test.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey on 06.05.2016.
 */
public abstract class HeaderFooterRecyclerAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;
    private List<T> data;

    private boolean mWithHeader;
    private boolean mWithFooter;

    public HeaderFooterRecyclerAdapter(boolean withHeader, boolean withFooter) {
        this.data = new ArrayList<>();
        mWithHeader = withHeader;
        mWithFooter = withFooter;
    }

    public void setData(List<T> data) {
        clearData(false);
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void clearData() {
        clearData(true);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case TYPE_ITEM:
                return getItemView(inflater, parent);
            case TYPE_HEADER:
                return getHeaderView(inflater, parent);
            case TYPE_FOOTER:
                return getFooterView(inflater, parent);
            default:
                throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
        }
    }

    @Override
    public int getItemCount() {
        int itemCount = data.size();
        if (mWithHeader) {
            itemCount++;
        }
        if (mWithFooter) {
            itemCount++;
        }
        return itemCount;
    }

    @Override
    public int getItemViewType(int position) {
        if (mWithHeader && isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        if (mWithFooter && isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    protected abstract RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent);

    protected abstract RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent);

    protected abstract RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent);

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == getItemCount() - 1;
    }

    protected T getItem(int position) {
        return mWithHeader ? data.get(position - 1) : data.get(position);
    }

    private void clearData(boolean notifyChanges) {
        if (!data.isEmpty()) {
            data.clear();
        }

        if (notifyChanges) {
            notifyDataSetChanged();
        }
    }
}
