package com.alexey.test.ui.adapter.viewholder;

import android.view.View;

/**
 * Created by Alexey on 07.05.2016.
 */
public abstract class BaseItemViewHolder<T> extends BaseViewHolder {

    public BaseItemViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindItem(T item);
}
