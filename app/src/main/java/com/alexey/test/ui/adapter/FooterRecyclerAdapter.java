package com.alexey.test.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alexey.test.R;
import com.alexey.test.ui.adapter.viewholder.FooterViewHolder;

/**
 * Created by Alexey on 07.05.2016.
 */
public abstract class FooterRecyclerAdapter<T> extends HeaderFooterRecyclerAdapter<T> {

    public FooterRecyclerAdapter() {
        super(false, true);
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        return new FooterViewHolder(inflater.inflate(R.layout.layout_footer, parent, false));
    }
}
