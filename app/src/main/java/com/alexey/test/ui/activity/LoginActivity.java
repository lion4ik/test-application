package com.alexey.test.ui.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.alexey.test.R;
import com.alexey.test.presenter.LoginActivityPresenter;
import com.alexey.test.ui.view.LoginActivityView;
import com.alexey.test.utils.AuthUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity<LoginActivityView, LoginActivityPresenter> implements LoginActivityView {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.web_view)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initWebView();
        getPresenter().authorizeVk();
    }

    @NonNull
    @Override
    public LoginActivityPresenter createPresenter() {
        return new LoginActivityPresenter();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setWebViewClient(WebViewClient webViewClient){
        webView.setWebViewClient(webViewClient);
    }

    @Override
    public void loadVkAuthorizeUrl() {
        webView.loadUrl(AuthUtils.getUrl(AuthUtils.APP_ID, AuthUtils.getSettings()));
    }

    @Override
    public void onAuthorized() {
        MainActivity.show(this);
    }

    private void initWebView() {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(false);
        webSettings.setSaveFormData(false);
        if (Build.VERSION.SDK_INT < 18)
            webSettings.setSavePassword(false);

        webView.clearCache(true);

        CookieSyncManager.createInstance(this);

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
    }
}

