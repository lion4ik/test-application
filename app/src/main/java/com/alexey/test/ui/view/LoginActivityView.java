package com.alexey.test.ui.view;

import android.webkit.WebViewClient;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Alexey on 03.05.2016.
 */
public interface LoginActivityView extends MvpView {

    void showProgress();

    void hideProgress();

    void setWebViewClient(WebViewClient webViewClient);

    void loadVkAuthorizeUrl();

    void onAuthorized();
}
