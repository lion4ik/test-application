package com.alexey.test.ui.view;

import com.alexey.test.model.market.VkGroupItem;
import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;

/**
 * Created by Alexey on 07.05.2016.
 */
public interface GroupsItemActivityView extends MvpView {

    void setGroupItems(List<VkGroupItem> groupItems);
}
