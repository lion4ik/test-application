package com.alexey.test.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.alexey.test.R;
import com.alexey.test.model.VkGroup;
import com.alexey.test.presenter.MainActivityPresenter;
import com.alexey.test.ui.adapter.GroupsRecyclerAdapter;
import com.alexey.test.ui.adapter.decoration.DividerItemDecoration;
import com.alexey.test.ui.view.MainActivityView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseSpiceMvpActivity<MainActivityView, MainActivityPresenter> implements MainActivityView, GroupsRecyclerAdapter.OnItemClickListener {

    @BindView(R.id.recyclerGroups)
    RecyclerView recyclerGroups;

    private GroupsRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initRecyclerGroups();
    }

    @NonNull
    @Override
    public MainActivityPresenter createPresenter() {
        return new MainActivityPresenter(getSpiceManager());
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPresenter().executeGroupsSearchRequest();
    }

    public static void show(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    private void initRecyclerGroups() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerGroups.setLayoutManager(layoutManager);
        recyclerGroups.addItemDecoration(new DividerItemDecoration(this));
        initRecyclerViewAdapter();
    }

    private void initRecyclerViewAdapter() {
        adapter = new GroupsRecyclerAdapter(this);
        recyclerGroups.setAdapter(adapter);
    }

    @Override
    public void setGroups(List<VkGroup> groups) {
        adapter.setData(groups);
    }

    @Override
    public void onItemClick(VkGroup group, int position) {
        GroupItemsActivity.show(this, -group.getId());
    }
}
