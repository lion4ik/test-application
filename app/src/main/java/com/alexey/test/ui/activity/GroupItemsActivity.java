package com.alexey.test.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.alexey.test.R;
import com.alexey.test.model.market.VkGroupItem;
import com.alexey.test.presenter.GroupsItemsActivityPresenter;
import com.alexey.test.ui.adapter.GroupItemsRecyclerAdapter;
import com.alexey.test.ui.view.GroupsItemActivityView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupItemsActivity extends BaseSpiceMvpActivity<GroupsItemActivityView, GroupsItemsActivityPresenter> implements GroupsItemActivityView {

    private static final int SPAN_COUNT = 2;
    private static final String EXTRA_OWNER_ID = "owner_id";

    @BindView(R.id.recycler)
    RecyclerView recyclerGroupItems;

    private Integer groupOwnerId;
    private GroupItemsRecyclerAdapter adapter;

    public static void show(Context context, Integer owner_id) {
        Intent intent = new Intent(context, GroupItemsActivity.class);
        intent.putExtra(EXTRA_OWNER_ID, owner_id);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        groupOwnerId = getIntent().getIntExtra(EXTRA_OWNER_ID, -1);
        setContentView(R.layout.recycler_view);
        ButterKnife.bind(this);
        initRecyclerGroups();
        getPresenter().executeGroupItemsRequest(groupOwnerId);
    }

    @NonNull
    @Override
    public GroupsItemsActivityPresenter createPresenter() {
        return new GroupsItemsActivityPresenter(getSpiceManager());
    }

    @Override
    public void setGroupItems(List<VkGroupItem> groupItems) {
        adapter.setData(groupItems);
    }

    private void initRecyclerViewAdapter() {
        adapter = new GroupItemsRecyclerAdapter();
        recyclerGroupItems.setAdapter(adapter);
    }

    private void initRecyclerGroups() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, SPAN_COUNT);
        recyclerGroupItems.setHasFixedSize(true);
        recyclerGroupItems.setLayoutManager(gridLayoutManager);
        initRecyclerViewAdapter();
    }
}
