package com.alexey.test.ui.adapter.viewholder;

import android.view.View;

import com.alexey.test.R;

import butterknife.BindView;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by Alexey on 07.05.2016.
 */
public class FooterViewHolder extends BaseViewHolder {
    @BindView(R.id.progress_bar)
    MaterialProgressBar progressBar;

    public FooterViewHolder(View itemView) {
        super(itemView);
    }

}
