package com.alexey.test.dagger;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.alexey.test.manager.VkAccountManager;
import com.alexey.test.model.VkAccount;
import com.alexey.test.network.VkWebViewAuthClient;
import com.jakewharton.retrofit.Ok3Client;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Client;

/**
 * Created by Alexey on 03.05.2016.
 */
@Module
public class AppModule {
    private Context appContext;

    public AppModule(Context appContext) {
        this.appContext = appContext;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(appContext);
    }

    @Provides
    @Singleton
    VkWebViewAuthClient provideVkWebViewAuthClient() {
        return new VkWebViewAuthClient();
    }

    @Provides
    @Singleton
    VkAccount provideVkAccount() {
        return new VkAccount();
    }

    @Provides
    @Singleton
    VkAccountManager provideVkAccountManager(SharedPreferences preferences, VkAccount vkAccount) {
        return new VkAccountManager(preferences, vkAccount);
    }

    @Provides
    @Singleton
    RequestInterceptor provideRequestInterceptor(final VkAccountManager vkAccountManager) {
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addEncodedQueryParam("access_token", vkAccountManager.getAccessToken());
                request.addEncodedQueryParam("v", "5.52");
            }
        };
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    RestAdapter.Log provideLog() {
        return new RestAdapter.Log() {
            @Override
            public void log(String message) {
                Log.d("Retrofit", message);
            }
        };
    }

    @Provides
    @Singleton
    Client provideClient(OkHttpClient okHttpClient) {
        return new Ok3Client(okHttpClient);
    }
}
