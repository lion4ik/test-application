package com.alexey.test;

import android.app.Application;

import com.alexey.test.dagger.AppComponent;
import com.alexey.test.dagger.AppModule;
import com.alexey.test.dagger.DaggerAppComponent;

/**
 * Created by Alexey on 03.05.2016.
 */
public class App extends Application{

    protected AppComponent appComponent;

    private static App instance;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        AppModule appModule = new AppModule(instance);
        appComponent = DaggerAppComponent.builder().appModule(appModule).build();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }
}
