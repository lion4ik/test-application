package com.alexey.test.network;

import com.alexey.test.model.VkGroupItems;
import com.alexey.test.model.VkGroupsList;

import retrofit.http.GET;
import retrofit.http.Query;


/**
 * Created by Alexey on 03.05.2016.
 */
public interface VkApi {

    @GET("/groups.search")
    BaseApiResponse<VkGroupsList> searchGroups(
            @Query("q") String q,
            @Query("market") String market
    );

    @GET("/market.get")
    BaseApiResponse<VkGroupItems> getGroupItems(
            @Query("owner_id") Integer owner_id
    );
}
