package com.alexey.test.network.listener;

import android.support.annotation.Nullable;
import android.util.Log;

import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.exception.RequestCancelledException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by Alexey on 05.05.2016.
 */
public abstract class BaseRequestListener<T> implements RequestListener<T> {

    private static final String TAG = "BaseRequestListener";

    @Override
    public final void onRequestFailure(SpiceException spiceException) {
        if (!isRequestCancelled(spiceException)) {
            Log.e(TAG, "Can't execute request", spiceException);
        }
        if (spiceException instanceof NoNetworkException) {
            onNoNetworkException();
        } else if (!(spiceException instanceof RequestCancelledException)) {
            onFailure(spiceException);
        }
    }

    private boolean isRequestCancelled(Exception e) {
        return e instanceof RequestCancelledException;
    }

    public abstract void onFailure(@Nullable Throwable cause);

    public void onNoNetworkException() {
        onFailure(null);
    }
}
