package com.alexey.test.network;

import com.alexey.test.App;
import com.alexey.test.R;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import javax.inject.Inject;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Client;

/**
 * Created by Alexey on 03.05.2016.
 */
public class NetworkService extends RetrofitGsonSpiceService {
    private static final int THREAD_COUNT = 2;

    @Inject
    RestAdapter.Log logger;

    @Inject
    RequestInterceptor requestInterceptor;

    @Inject
    Client client;

    @Override
    public void onCreate() {
        App.getInstance().getAppComponent().inject(this);
        super.onCreate();
        addRetrofitInterface(VkApi.class);
    }

    @Override
    protected String getServerUrl() {
        return getString(R.string.vk_api_url);
    }

    @Override
    public int getThreadCount() {
        return THREAD_COUNT;
    }

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RestAdapter.Builder builder = super.createRestAdapterBuilder();
        builder.setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(logger)
                .setClient(client)
                .setRequestInterceptor(requestInterceptor);
        return builder;
    }
}
