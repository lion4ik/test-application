package com.alexey.test.network.request;

import com.alexey.test.network.VkApi;

/**
 * Created by Alexey on 05.05.2016.
 */
public abstract class VkApiRequest extends BaseApiRequest<VkApi> {

    public VkApiRequest() {
        super(VkApi.class);
    }

}
