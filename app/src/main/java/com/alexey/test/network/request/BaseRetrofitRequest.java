package com.alexey.test.network.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by Alexey on 05.05.2016.
 */
public abstract class BaseRetrofitRequest<T, A>  extends RetrofitSpiceRequest<T, A> {

    public BaseRetrofitRequest(Class<T> clazz, Class<A> retrofitedInterfaceClass) {
        super(clazz, retrofitedInterfaceClass);
    }
}
