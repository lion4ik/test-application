package com.alexey.test.network.request;

import com.alexey.test.network.BaseApiResponse;

/**
 * Created by Alexey on 07.05.2016.
 */
public class GroupItemsRequest extends VkApiRequest {
    private Integer owner_id;

    public GroupItemsRequest(Integer owner_id) {
        this.owner_id = owner_id;
    }

    @Override
    public BaseApiResponse<?> getApiResponse() throws Exception {
        return getService().getGroupItems(owner_id);
    }
}
