package com.alexey.test.network.request;

import com.alexey.test.network.BaseApiResponse;

/**
 * Created by Alexey on 05.05.2016.
 */
public class GroupsSearchRequest extends VkApiRequest {
    private String q;

    public GroupsSearchRequest(String q){
        this.q = q;
    }

    @Override
    public BaseApiResponse<?> getApiResponse() throws Exception {
        return getService().searchGroups(q, "1");
    }
}
