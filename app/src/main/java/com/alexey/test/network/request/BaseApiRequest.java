package com.alexey.test.network.request;

import com.alexey.test.network.BaseApiResponse;
import com.alexey.test.network.ResponseWrapper;

/**
 * Created by Alexey on 05.05.2016.
 */
public abstract class BaseApiRequest<A> extends BaseRetrofitRequest<ResponseWrapper, A> {

    public BaseApiRequest(Class<A> retrofitInterfaceClass) {
        super(ResponseWrapper.class, retrofitInterfaceClass);
    }

    @Override
    public ResponseWrapper loadDataFromNetwork() throws Exception {
        return new ResponseWrapper(getApiResponse());
    }

    public abstract BaseApiResponse<?> getApiResponse() throws Exception;
}
