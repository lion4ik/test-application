package com.alexey.test.network;

/**
 * Created by Alexey on 05.05.2016.
 */
public class ResponseWrapper {

    private final BaseApiResponse<?> response;

    public ResponseWrapper(BaseApiResponse<?> response) {
        this.response = response;
    }

    @SuppressWarnings("unchecked")
    public <T> T getResponse() {
        return (T) response;
    }
}
