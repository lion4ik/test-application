package com.alexey.test.network;

import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.alexey.test.utils.AuthUtils;

/**
 * Created by Alexey on 03.05.2016.
 */
public class VkWebViewAuthClient extends WebViewClient {
    private static final String TAG = "VkWebViewAuthClient";

    private AuthListener authListener;

    public void setAuthListener(AuthListener authListener) {
        this.authListener = authListener;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        authListener.onFinishLoading();
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        authListener.onStartLoading();
        parseUrl(url);
    }

    private void parseUrl(String url) {
        if (authListener != null && url.startsWith(AuthUtils.REDIRECT_URL) && !url.contains("error=")) {
            try {
                String[] auth = AuthUtils.parseRedirectUrl(url);
                authListener.onAuthorized(auth[0], auth[1]);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public interface AuthListener {
        void onStartLoading();

        void onFinishLoading();

        void onAuthorized(String accessToken, String userId);
    }
}
