package com.alexey.test.network;

/**
 * Created by Alexey on 04.05.2016.
 */
public class BaseApiResponse<T>{

    private T response;

    public T getResult(){
        return response;
    }
}