package com.alexey.test.network.listener;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.alexey.test.network.BaseApiResponse;
import com.alexey.test.network.ResponseWrapper;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by Alexey on 05.05.2016.
 */
public abstract class ApiRequestListener<T> extends BaseRequestListener<ResponseWrapper>{
    private static final String TAG = "ApiRequestListener2";

    @Override
    public void onFailure(@Nullable Throwable cause) {
        String errorMessage = null;
        if (cause != null) {
            errorMessage = cause.getMessage();
        }
        onErrorWithCode(errorMessage, null);
    }

    /**
     * @param wrapper result of the request. Can be null when returned from cache.
     *                @see SpiceManager#getFromCache(Class, Object, long, RequestListener)
     */
    @Override
    @CallSuper
    public void onRequestSuccess(@Nullable ResponseWrapper wrapper) {
        if (wrapper == null) {
            onErrorWithCode(null, null);
            return;
        }
        BaseApiResponse<T> response = wrapper.getResponse();
        if (response == null) {
            onError(null);
        } else {
            onDataReceived(response.getResult());
        }
    }

    public abstract void onDataReceived(T data);

    public void onError(String errorMessage) {
        // empty by default
    }


    public void onErrorWithCode(@Nullable String errorMessage, @Nullable String errorCode) {
        onError(errorMessage);
    }
}
