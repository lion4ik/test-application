package com.alexey.test.utils;

import android.util.Log;

import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexey on 03.05.2016.
 */
public final class AuthUtils {
    private static final String TAG = "AuthUtils";
    public static final String REDIRECT_URL = "https://oauth.vk.com/blank.html";
    public static final String API_VERSION = "5.52";
    public static final String APP_ID = "3366842";

    public static String getUrl(String api_id, String settings) {
        String url = "https://oauth.vk.com/authorize?client_id=" + api_id + "&display=mobile&scope=" + settings + "&redirect_uri=" + URLEncoder.encode(REDIRECT_URL) + "&response_type=token"
                + "&v=" + URLEncoder.encode(API_VERSION);
        return url;
    }

    public static String getSettings() {
        //http://vk.com/dev/permissions
        return "groups,market";//"notify,friends,photos,audio,video,docs,status,notes,pages,wall,groups,messages,offline,notifications";
    }

    public static String[] parseRedirectUrl(String url) throws IllegalArgumentException {
        //url is something like http://api.vkontakte.ru/blank.html#access_token=66e8f7a266af0dd477fcd3916366b17436e66af77ac352aeb270be99df7deeb&expires_in=0&user_id=7657164
        String access_token = extractPattern(url, "access_token=(.*?)&");
        Log.i(TAG, "access_token=" + access_token);
        String user_id = extractPattern(url, "user_id=(\\d*)");
        Log.i(TAG, "user_id=" + user_id);
        if (user_id == null || user_id.length() == 0 || access_token == null || access_token.length() == 0)
            throw new IllegalArgumentException("Failed to parse redirect url " + url);
        return new String[]{access_token, user_id};
    }

    public static String extractPattern(String string, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(string);
        if (!m.find())
            return null;
        return m.toMatchResult().group(1);
    }
}
