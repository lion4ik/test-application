package com.alexey.test.manager;

import android.content.SharedPreferences;

import com.alexey.test.model.VkAccount;

import javax.inject.Inject;

/**
 * Created by Alexey on 03.05.2016.
 */
public class VkAccountManager {
    private static final String ACCESS_TOKEN = "accessToken";
    private static final String USER_ID = "userId";

    private SharedPreferences preferences;

    private VkAccount vkAccount;

    @Inject
    public VkAccountManager(SharedPreferences preferences, VkAccount vkAccount) {
        this.preferences = preferences;
        this.vkAccount = vkAccount;
    }

    public void saveAccountData(String accessToken, String userId) {
        vkAccount.setAccessToken(accessToken);
        vkAccount.setUserId(userId);
        preferences.edit()
                .putString(ACCESS_TOKEN, accessToken)
                .putString(USER_ID, userId)
                .commit();
    }

    public void restoreAccountData() {
        vkAccount.setAccessToken(preferences.getString(ACCESS_TOKEN, null));
        vkAccount.setUserId(preferences.getString(USER_ID, null));
    }

    public boolean hasSavedAccountData() {
        return preferences.contains(ACCESS_TOKEN) && preferences.contains(USER_ID);
    }

    public String getAccessToken(){
        return vkAccount.getAccessToken();
    }

    public String getUserId(){
        return vkAccount.getUserId();
    }
}
